#include <NewPing.h>
#include "DHT.h"
#include <RCSwitch.h>
#include <WiFi.h>

#define TRIGGER_PIN  25  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     27  // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 400 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
#define DHTPIN 14    // what digital pin we're connected to
#define DHTTYPE DHT11   // DHT 11

DHT dht(DHTPIN, DHTTYPE);
RCSwitch mySwitch = RCSwitch();

//hard data
int relayPin = 21;
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
const long interval = 2000;           // interval at which to collect data (milliseconds)
unsigned long previousMillis = 0;        // will store last time LED was updated
const long PING_INTERVAL = 10000;
unsigned long pprev = 0;
const char* ssid     = "";
const char* password = "";
float h = 0.0;
float t = 0.0;
int cm = 0;
int garage_open = 13593235;
int garage_close = 1861260;
int garage_air = 9250784;
int garage_stop = 7380824;

//changed data
int lastREC = 0;
bool processing = false;
WiFiServer server(80);
void setup() {
  pinMode(relayPin, OUTPUT);
  Serial.begin(115200); // Open serial monitor at 115200 baud to see ping results.
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  server.begin();
  mySwitch.enableReceive(5);
}
void cycle() {
  digitalWrite(relayPin, HIGH);
  delay(30);
  digitalWrite(relayPin, LOW);
}
void loop() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;
    // Reading temperature or humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
    h = dht.readHumidity();
    // Read temperature as Celsius (the default)
    t = dht.readTemperature();
  }
  if (currentMillis - pprev >= PING_INTERVAL) {
    int temp_cm = sonar.ping_cm();
    if(!(cm - temp_cm >= 50)){
    if (processing) {
      Serial.println(temp_cm);
      if (temp_cm > cm) {
        //garage is coming down
        //its okay if it's garage_close
        if (lastREC == garage_air) {

        } else if (lastREC == garage_open) {
          //yes, we just cycle it to open instead.
          cycle();
        }
      } else {
        //garage is coming up
        //its  okay if it's garage_open
        if (lastREC == garage_air) {

        } else if (lastREC == garage_close) {
          //yes, we just cycle it to close instead.
          cycle();
        }
      }
      if (temp_cm == cm) {
       // processing = false;
      }
    }
      if (temp_cm > 0) {
        cm = temp_cm;
      }
    }else if(cm == 0){
      if (temp_cm > 0) {
        cm = temp_cm;
      }
      }
    }
    if (mySwitch.available()) {

      int value = mySwitch.getReceivedValue();

      if (value == 0 || value != garage_stop && value != garage_air && value != garage_open && value != garage_close) {
        Serial.print("Unknown encoding");
      } else if (!processing || value == garage_stop) {
        Serial.print("Received ");
        Serial.print(value);
        Serial.println();
        if (value == garage_stop) {
          cycle();
          processing = false;
        } else{
          cycle();
          processing = true;
        }
        lastREC = value;
      }

      mySwitch.resetAvailable();
    }
    WiFiClient client = server.available();   // listen for incoming clients

    if (client) {                             // if you get a client,
      Serial.println("New Client.");           // print a message out the serial port
      String currentLine = "";                // make a String to hold incoming data from the client
      while (client.connected()) {            // loop while the client's connected
        if (client.available()) {             // if there's bytes to read from the client,
          char c = client.read();             // read a byte, then
          //Serial.write(c);                    // print it out the serial monitor
          if (c == '\n') {                    // if the byte is a newline character

            // if the current line is blank, you got two newline characters in a row.
            // that's the end of the client HTTP request, so send a response:
            if (currentLine.length() == 0) {
              // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
              // and a content-type so the client knows what's coming, then a blank line:
              client.println("HTTP/1.1 200 OK");
              client.println("Content-type:text/html");
              client.println();
              //body
              if (!(isnan(h) || isnan(t))) {
                client.print("Humidty: ");
                client.print(h);
                client.print(" Temperature: ");
                client.print(t);
                client.print(" celsius");
              } else {
                Serial.println("Failed to read from DHT sensor!");
              }
              client.print("<br>Distance to garage port: ");
              client.print(cm);
              client.print("cm");
              client.print("<br>Wanted garage position in cm:");
              client.print("<br>");
              client.print("<form action=\"/action_page.php\" method=\"get\">");
              client.print("<input type=\"range\" min=\"10\" max=\"300\" value=\"10\" step=\"5\" onchange=\"showValue(this.value)\" />");
              client.print("<span id=\"range\">10</span>");
              client.print("<input type=\"submit\" value=\"Submit\">");
              client.print("</form>");
              client.print("<script type=\"text/javascript\">function showValue(newValue){document.getElementById(\"range\").innerHTML=newValue;}</script>");
              client.print("Last received value: ");
              client.print(lastREC);
              // The HTTP response ends with another blank line:
              client.println();
              // break out of the while loop:
              break;
            } else {    // if you got a newline, then clear currentLine:
              // Check to see if the client request was "GET /H" or "GET /L":
              Serial.println(currentLine);
              if (currentLine.endsWith("GET /H")) {
                digitalWrite(relayPin, HIGH);               // GET /H turns the LED on
              }
              if (currentLine.endsWith("GET /L")) {
                digitalWrite(relayPin, LOW);                // GET /L turns the LED off
              }
              currentLine = "";
            }
          } else if (c != '\r') {  // if you got anything else but a carriage return character,
            currentLine += c;      // add it to the end of the currentLine
          }
        }

      }

      // close the connection:
      client.stop();
      Serial.println("Client Disconnected.");
    }
  }
